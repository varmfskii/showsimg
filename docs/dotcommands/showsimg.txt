showsimg by Theodore (Alex) Evans 2020-2021

Usage: .showsimg [<options>] <filename>
options:

 -2: Layer 2 256x192x8
 -3: Layer 2 320x256x8
 -6: Layer 2 640x256x4
 -R: Radastan 128x96x4
 -M: MLT HiColor
 -c: Timex Hicol 256x192/8x1
 -h: This help
 -l: LoRes 128x96x8
 -m: MC HiColor
 -n: NXI: Layer 2
 -r: Timex HiRes 512x192x1
 -u: ULA 256x192/8x8
 -x: Do not wait, do not restore

Shows images of various formats. If not given command line options
specifying type, it will attempt to guess the image type from a
combination of the file extension and file size. In addition to
standard versions of the file formats, many are augmented with palette
data.

File format information:

MC: Timex High Color screen dump (256x192x1/8x1)
  * 12288 bytes of image data (pixels and attributes in normal order)
  * Optionally:
    - 16 or 32 bytes of palette data or
    - 33-514 bytes of ULANext extension and palette data or
    - 64 bytes of ULAplus palette data

MLT: Timex High Color screen dump (256x192x1/8x1)
  * 12288 bytes of image data (pixels in memory order, attributes
    normal order)
  * Optionally:
    - 16 or 32 bytes of palette data or
    - 33-514 bytes of ULANext extension and palette data or
    - 64 bytes of ULAplus palette data

NXI: ZX Spectrum Next Layer 2 screen dump (256x192x8)
  * Optionally: 256 or 512 bytes palette data
  * 49152 bytes of image data

SCR: ZX Spectrum ULA screen dump (256x192x1/8x8)
  * 6144 bytes of image data (no colour attribute data)
  * Optionally:
    - 16 or 32 bytes of palette data or
    - 33-514 bytes of ULANext extension and palette data or
    - 64 bytes of ULAplus palette data
  
SCR: ZX Spectrum ULA screen dump (256x192x1/8x8)
  * 6912 bytes of image data
  
SHC: Timex High Color screen dump (256x192x1/8x1)
  * 12288 bytes of image data (memory order) 
  * Optionally:
    - 16 or 32 bytes of palette data or
    - 33-514 bytes of ULANext extension and palette data or
    - 64 bytes of ULAplus palette data
 
SHR: Times High Resolution screen dump (512x192x1)
  * 12288 bytes of image data
  * Optionaly 1 byte to select colours
  
SL2: ZX Spectrum Next Layer 2 screen dump (256x192x8)
  * 49152 bytes of image data (left to right, top to bottom)
  * Optionally: 256 or 512 bytes of ZX Next palette data

SL2: ZX Spectrum Next Layer 2 screen dump (320x256x8)
  * 81920 bytes of image data (top to bottom, left to right)
  * Optionally: 256 or 512 bytes of ZX Next palette data

SL2: ZX Spectrum Next Layer 2 screen dump (640x256x4)
  * 81920 bytes of image data (each byte is a left to right pixel
    pair, bytes are arranged top to bottom, left to right)
  * Optionally: 16 or 32 bytes of ZX Next palette data

SL2: Radastan Screen dump (128x96x4)
  * 6144 bytes of image data
  * Optionally: 16 or 32 bytes of palette data

SLR: ZX Spectrum Next LoRes screen dump (128x96x8)
  * 12288 bytes of image data
  * Optionally: 256 or 512 bytes of palette data

Extension information:

ULAplus extension:
  * 64 bytes of ULAplus palette information

ULANext extension:
  * 1 byte ULANext attribute ($01, $03, $07, $0F, $1F, $3F, $7F, or $FF)
  * 32 to 513 bytes of palette data depending on attribute value
    - Attribute $01: 130 or 260 bytes of palette data (2/128 colours)
    - Attribute $03: 68 or 136 bytes of palette data (4/64 colours)
    - Attribute $07: 40 or 80 bytes of palette data (8/32 colours)
    - Attribute $0F: 32 or 64 bytes of palette data (16/16 colours)
    - Attribute $1F: 40 or 80 bytes of palette data (32/8 colours)
    - Attribute $3F: 68 or 136 bytes of palette data (64/4 colors)
    - Attribute $7F: 130 or 260 bytes of palette data (128/2 colours)
    - Attribute $FF: 257 or 513 bytes of palette data (256/1 colours,
      paper colour is always 8-bit)
